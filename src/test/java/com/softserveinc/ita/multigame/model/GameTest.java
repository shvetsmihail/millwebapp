package com.softserveinc.ita.multigame.model;


import com.softserveinc.ita.multigame.model.engine.GameResultCode;
import com.softserveinc.ita.multigame.model.engine.millEngine.MillGameEngine;
import com.softserveinc.ita.multigame.services.GsonParserService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class GameTest {
    private MillGameEngine mill = mock(MillGameEngine.class);
    private Player player = mock(Player.class);
    private Game game;


    @Before
    public void setUp() {
        when(player.getLogin()).thenReturn("1");
        game = new Game(player);
        game.setGame(mill);
    }

    @Test
    public void whenGetId(){
        when(mill.getId()).thenReturn(1L);

        assertSame(1L, game.getId());
        verify(mill).getId();
    }

    @Test
    public void whenJoinToGame(){
        when(mill.getSecondPlayer()).thenReturn(null);

        assertTrue(game.joinToGame(player));
        verify(mill).getSecondPlayer();
        verify(mill).setSecondPlayer("1");
        verify(player, times(2)).getLogin();

    }

    @Test
    public void whenMakeTurn(){
        when(mill.makeTurn("1", "p5")).thenReturn(true);

        assertTrue(game.makeTurn(player, "p5"));
        verify(mill).makeTurn("1", "p5");
        verify(player, times(2)).getLogin();
    }

    @Test
    public void whenGetTipsMethodInvokedAndGameIsFinished(){
        when(mill.isFinished()).thenReturn(true);
        when(mill.getTheWinner()).thenReturn("1");

        assertEquals(">White: Game Over. Congratulation 1", game.getTips(player));
        verify(mill).isFinished();
        verify(mill).getTheWinner();
    }

    @Test
    public void whenGetTipsMethodInvokedAndNotCurrentPlayerMakeTurn(){
        when(mill.isFinished()).thenReturn(false);
        when(mill.isStarted()).thenReturn(true);
        when(mill.isCurrentPlayer("1")).thenReturn(false);

        assertEquals(">White: Opponents turn", game.getTips(player));
        verify(mill).isFinished();
        verify(mill).isStarted();
        verify(mill).isCurrentPlayer("1");
        verify(player, times(2)).getLogin();
    }

    @Test
    public void whenGetTipsMethodInvokedAndGameIsNotStarting(){
        when(mill.isFinished()).thenReturn(false);
        when(mill.isStarted()).thenReturn(false);
        when(mill.isCurrentPlayer("1")).thenReturn(true);

        assertEquals(">White: Please wait for your opponent", game.getTips(player));
        verify(mill).isFinished();
        verify(mill).isStarted();
        verify(mill, never()).isCurrentPlayer("1");
        verify(player, times(1)).getLogin();
    }

    @Test
    public void whenGetTipsMethodInvokedAndGetResultCodeBatTurnLogic(){
        when(mill.isFinished()).thenReturn(false);
        when(mill.isStarted()).thenReturn(true);
        when(mill.isCurrentPlayer("1")).thenReturn(true);
        when(mill.getResultCode()).thenReturn(GameResultCode.BAD_TURN_LOGIC);

        assertEquals(">White: Wrong command. Read rules and try again!", game.getTips(player));
        verify(mill).isFinished();
        verify(mill).isStarted();
        verify(mill).isCurrentPlayer("1");
        verify(player, times(2)).getLogin();
        verify(mill).getResultCode();
    }

    @Test
    public void whenGetTipsMethodInvokedAndGetResultCodeBatTurnSyntax(){
        when(mill.isFinished()).thenReturn(false);
        when(mill.isStarted()).thenReturn(true);
        when(mill.isCurrentPlayer("1")).thenReturn(true);
        when(mill.getResultCode()).thenReturn(GameResultCode.BAD_TURN_SYNTAX);

        assertEquals(">White: Mistake in command. Please try again!", game.getTips(player));
        verify(mill).isFinished();
        verify(mill).isStarted();
        verify(mill).isCurrentPlayer("1");
        verify(player, times(2)).getLogin();
        verify(mill).getResultCode();
    }

    @Test
    public void whenGetTipsMethodInvokedAndGetResultCodeNotBatTurnLogicAndNotBatTurnSyntax(){
        when(mill.isFinished()).thenReturn(false);
        when(mill.isStarted()).thenReturn(true);
        when(mill.isCurrentPlayer("1")).thenReturn(true);
        when(mill.getResultCode()).thenReturn(GameResultCode.BAD_PLAYER);

        assertEquals(">White: Your turn ", game.getTips(player));
        verify(mill).isFinished();
        verify(mill).isStarted();
        verify(mill).isCurrentPlayer("1");
        verify(player, times(2)).getLogin();
        verify(mill).getResultCode();
    }

    @Test
    public void whenGetTipsMethodInvokedAndCurrentPlayerMakeTurn(){
        when(mill.isFinished()).thenReturn(false);
        when(mill.isStarted()).thenReturn(true);
        when(mill.isCurrentPlayer("1")).thenReturn(true);

        assertEquals(">White: Your turn ", game.getTips(player));
        verify(mill).isFinished();
        verify(mill).isStarted();
        verify(mill).isCurrentPlayer("1");
        verify(player, times(2)).getLogin();
    }


    @Test
    public void whenGetColorAndFirstPlayerMakeTurn(){
        assertEquals("White" ,game.getColor(player));
    }

    @Test
    public void whenGetColorAndSecondPlayerMakeTurn(){
        assertEquals("Black" ,game.getColor(mock(Player.class)));

    }

    @Test
    public void whenGetStones(){
        List<String> s = new ArrayList<>();
        when(mill.getStonesList()).thenReturn(s);
        assertEquals(s, GsonParserService.fromJson(game.getStonesInHand(), s.getClass()) );
        verify(mill).getStonesList();
    }





}
