package com.softserveinc.ita.multigame.model.services;


import com.softserveinc.ita.multigame.services.PasswordEncryptionService;
import com.softserveinc.ita.multigame.services.impl.PasswordEncryptionServiceImpl;
import org.junit.Test;

import static org.junit.Assert.*;


public class PasswordEncryptionServiceTest {
    PasswordEncryptionService pes = PasswordEncryptionServiceImpl.getInstance();
    String password1 = "111";
    String password2 = "qwe123rtyu456ffgufdidfj";

    @Test
    public void EncryptAndCheckPassword(){
        String encryptPassword1 = pes.encryptPassword(password1);
        String encryptPassword2 = pes.encryptPassword(password2);

        assertTrue(pes.checkPassword(password1, encryptPassword1));
        assertTrue(pes.checkPassword(password2, encryptPassword2));

        assertFalse(pes.checkPassword(password1, encryptPassword2));
        assertFalse(pes.checkPassword(password2, encryptPassword1));
    }

    @Test
    public void CheckThatAllEncryptPasswordsHaveSameLenth(){
        String encryptPassword1 = pes.encryptPassword(password1);
        String encryptPassword2 = pes.encryptPassword(password2);

        assertNotSame(password1.length(), password2.length());
        assertSame(encryptPassword1.length(), encryptPassword2.length());
    }




}
