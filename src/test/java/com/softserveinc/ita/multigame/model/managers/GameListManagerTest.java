package com.softserveinc.ita.multigame.model.managers;

import com.softserveinc.ita.multigame.dao.GameHistoryDao;
import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.managers.impl.GameManagerImpl;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class GameListManagerTest {
    private GameManagerImpl manager = GameManagerImpl.getInstance();
    private static final Player player1 = mock(Player.class);
    private static final Player player2 = mock(Player.class);
    private static Game game1 = mock(Game.class);
    private GameHistoryDao gameHistoryDao = mock(GameHistoryDao.class);
    private GameHistory gameHistory = mock(GameHistory.class);



    @Before
    public void setUp() {
        Map<Long, Game> games = new ConcurrentHashMap<>();
        games.put(1L,game1);

        try {
            Field field1 = manager.getClass().getDeclaredField("games");
            field1.setAccessible(true);
            field1.set(manager, games);
        } catch (Exception e) {
            fail("Field \"games\" is not accessible");
        }

        when(game1.getId()).thenReturn(1L);
        when(player1.getId()).thenReturn(1L);
        when(player1.getId()).thenReturn(2L);
    }

    @Test
    public void managerShouldBeSingleton() {
        GameManagerImpl newManager = GameManagerImpl.getInstance();
        assertSame(newManager, manager);

    }

    @Test
    public void managerCoulsNotCreateGame() {
        assertTrue(manager.createGame(player1));
    }

    @Test
    public void nullCoulsNotCreateGame() {
        assertFalse(manager.createGame(null));
    }

    @Test
    public void managerCouldDeleteGameIfItExist(){
        assertTrue(manager.deleteGame(1L));
    }

    @Test
    public void managerCouldNotDeleteGameIfItNotExist(){
        assertFalse(manager.deleteGame(100L));
        assertFalse(manager.deleteGame(null));
    }


    @Test
    public void managerCouldGetGameifItWasDeleted() {
        Game g1 = manager.getGameById(1L);
        manager.deleteGame(1L);
        assertEquals(0, manager.getAllIDs().size());

        Game g2 = manager.getGameById(1L);
        assertEquals(g1, g2);
    }

    @Test
    public void gameShouldBeDeleteIfItWasFinished() {
        when(game1.isFinished()).thenReturn(true);
        Game g1 = manager.getGameById(1L);
        assertEquals(0, manager.getAllIDs().size());

        Game g2 = manager.getGameById(1L);
        assertEquals(g1, g2);
    }

    @Test
    public void gameShouldNotBeDetIfItWasNotCreate() {
        Game g1 = manager.getGameById(11L);
        assertNull(g1);
    }


    @Test
    public void countOfCreatedGamesByPlayer1MustBeOne() {
        when(game1.getFirstPlayer()).thenReturn(player1);
        when(game1.getSecondPlayer()).thenReturn(null);

        List<Long> createdGameIDs = manager.getCreatedGameIDs(player1);

        assertEquals(1, createdGameIDs.size());
        assertSame(game1, manager.getGameById(createdGameIDs.get(0)));
        assertEquals(player1, game1.getFirstPlayer());
    }

    @Test
    public void countOfCreatedGamesByPlayer1MustBeZero() {
        when(game1.getFirstPlayer()).thenReturn(player1);
        when(game1.getSecondPlayer()).thenReturn(player2);

        List<Long> createdGameIDs = manager.getCreatedGameIDs(player1);

        assertEquals(0, createdGameIDs.size());
    }

    @Test
    public void countOfPlayingGamesByPlayer1MustBeOne() {
        when(game1.getFirstPlayer()).thenReturn(player1);
        when(game1.getSecondPlayer()).thenReturn(player2);

        List<Long> playingGameIDs = manager.getPlayingGameIDs(player1);

        assertEquals(1, playingGameIDs.size());
        assertSame(game1, manager.getGameById(playingGameIDs.get(0)));
        assertEquals(player1, game1.getFirstPlayer());
        assertEquals(player2, game1.getSecondPlayer());

        when(game1.getFirstPlayer()).thenReturn(null);
        when(game1.getSecondPlayer()).thenReturn(player1);

        playingGameIDs = manager.getPlayingGameIDs(player1);

        assertEquals(1, playingGameIDs.size());
        assertSame(game1, manager.getGameById(playingGameIDs.get(0)));
    }

    @Test
    public void countOfPlayingGamesByPlayer1MustBeZero() {
        when(game1.getFirstPlayer()).thenReturn(player1);
        when(game1.getSecondPlayer()).thenReturn(null);
        List<Long> playingGameIDs = manager.getPlayingGameIDs(player1);
        assertEquals(0, playingGameIDs.size());

        when(game1.getFirstPlayer()).thenReturn(player2);
        when(game1.getSecondPlayer()).thenReturn(player2);
        playingGameIDs = manager.getPlayingGameIDs(player1);
        assertEquals(0, playingGameIDs.size());

    }


    @Test
    public void countOfWaitingGamesForPlayer1MustBeOne() {
        when(game1.getFirstPlayer()).thenReturn(player2);
        when(game1.getSecondPlayer()).thenReturn(player2);

        List<Long> waitingGameIDs = manager.getWaitingGameIDs(player1);

        assertEquals(1, waitingGameIDs.size());
        assertSame(game1, manager.getGameById(waitingGameIDs.get(0)));
    }

    @Test
    public void countOfWaitingGamesForPlayer1MustBeZero() {
        when(game1.getFirstPlayer()).thenReturn(player1);
        when(game1.getSecondPlayer()).thenReturn(player2);
        List<Long> waitingGameIDs = manager.getWaitingGameIDs(player1);
        assertEquals(0, waitingGameIDs.size());

        when(game1.getFirstPlayer()).thenReturn(player2);
        when(game1.getSecondPlayer()).thenReturn(player1);
        waitingGameIDs = manager.getWaitingGameIDs(player1);
        assertEquals(0, waitingGameIDs.size());

        when(game1.getFirstPlayer()).thenReturn(null);
        when(game1.getSecondPlayer()).thenReturn(player1);
        waitingGameIDs = manager.getWaitingGameIDs(player1);
        assertEquals(0, waitingGameIDs.size());

    }

    @Test
    public void managerCouldGetGameHistory() {

        mockGameHistoryDao();

        when(gameHistoryDao.getById(1L)).thenReturn(gameHistory);
        when(gameHistory.getGameId()).thenReturn(1L);

        GameHistory gh = manager.getGameHistory(1L);

        assertEquals(gh, gameHistory);
    }

    @Test
    public void managerCouldGetPlaterListOFGameHistory() {

        mockGameHistoryDao();

        List<GameHistory> list = new ArrayList<>();
        list.add(gameHistory);
        when(gameHistoryDao.getAllGames(player1)).thenReturn(list);
        List<GameHistory> list1 = manager.getGameHistories(player1);
        assertSame(1, list1.size());
        assertEquals(list, list1);
    }

    @Test
    public void managerCouldDeleteGameHistories() {

        mockGameHistoryDao();

        List<GameHistory> list = new ArrayList<>();
        list.add(gameHistory);
        when(gameHistoryDao.getAllGames(player1)).thenReturn(list);
        when(gameHistoryDao.delete(gameHistory)).thenReturn(true);


        assertTrue(manager.deleteGameHistory(player1));
    }

    private void mockGameHistoryDao() {
        try {
            Field field1 = manager.getClass().getDeclaredField("gameHistoryDao");
            field1.setAccessible(true);
            field1.set(manager, gameHistoryDao);
        } catch (Exception e) {
            fail("Field \"gameHistoryDao\" is not accessible");
        }
    }
}
