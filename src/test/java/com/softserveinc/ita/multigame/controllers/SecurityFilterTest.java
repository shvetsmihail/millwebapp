package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.Player;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class SecurityFilterTest {
    private SecurityFilter securityFilter = new SecurityFilter();
    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private HttpSession session = mock(HttpSession.class);
    private FilterChain filterChain = mock(FilterChain.class);

    @Before
    public void setUp(){

    }

    @Test
    public void SecurityFilterDoFilterTestPathToLogin() throws IOException, ServletException {
        String servletPath = "/login";
        when(request.getServletPath()).thenReturn(servletPath);
        securityFilter.doFilter(request, response, filterChain);

        verify(request).getServletPath();
        verify(filterChain).doFilter(request, response);

    }

    @Test
    public void SecurityFilterDoFilterTestPathToRegistration() throws IOException, ServletException {
        String servletPath = "/registration";
        when(request.getServletPath()).thenReturn(servletPath);
        securityFilter.doFilter(request, response, filterChain);

        verify(request).getServletPath();
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void SecurityFilterDoFilterTestPathToCSSResource() throws IOException, ServletException {
        String servletPath = "/registration.css";
        when(request.getServletPath()).thenReturn(servletPath);
        securityFilter.doFilter(request, response, filterChain);

        verify(request).getServletPath();
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void SecurityFilterDoFilterTestPathToPngResource() throws IOException, ServletException {
        String servletPath = "/registration.png";
        when(request.getServletPath()).thenReturn(servletPath);
        securityFilter.doFilter(request, response, filterChain);

        verify(request).getServletPath();
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void SecurityFilterDoFilterTestPlayerNotNull() throws IOException, ServletException {
        String servletPath = "/gameList";
        when(request.getServletPath()).thenReturn(servletPath);

        Player player = mock(Player.class);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("player")).thenReturn(player);

        securityFilter.doFilter(request, response, filterChain);

        verify(request).getServletPath();
        verify(request).getSession();
        verify(session).getAttribute("player");
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void SecurityFilterDoFilterTestPlayerWasNull() throws IOException, ServletException {
        String servletPath = "/gameList";
        when(request.getServletPath()).thenReturn(servletPath);

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("player")).thenReturn(null);
        when(request.getContextPath()).thenReturn("path");

        securityFilter.doFilter(request, response, filterChain);

        verify(request).getServletPath();
        verify(request).getSession();
        verify(session).getAttribute("player");
        verify(response).sendRedirect("path/login");
    }

}
