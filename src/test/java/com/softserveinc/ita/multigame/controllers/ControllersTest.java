package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.*;
import com.softserveinc.ita.multigame.model.managers.GameManager;
import com.softserveinc.ita.multigame.model.managers.PlayerManager;
import com.softserveinc.ita.multigame.services.GsonParserService;
import com.softserveinc.ita.multigame.services.PasswordEncryptionService;
import org.junit.Test;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

public class ControllersTest {
    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private HttpSession session = mock(HttpSession.class);

    @Test
    public void CreateGameControllerTest() throws ServletException, IOException {
        Player player = mock(Player.class);
        GameManager gameManager = mock(GameManager.class);
        CreateGameController c = new CreateGameController();
        mockField(c, gameManager, "gameManager");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("player")).thenReturn(player);
        when(gameManager.createGame(player)).thenReturn(true);

        c.doPost(request, response);

        verify(request).getSession();
        verify(session).getAttribute("player");
        verify(gameManager).createGame(player);
        verify(response).sendRedirect("gameList");
    }

    @Test
    public void ExitControllerTest() throws ServletException, IOException {
        ExitController c = new ExitController();
        when(request.getSession()).thenReturn(session);

        c.doGet(request, response);

        verify(request).getSession();
        verify(session).removeAttribute("player");
        verify(response).sendRedirect("login");

    }

    @Test
    public void GameControllerTestCreatingGame() throws ServletException, IOException {
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        Player p1 = mock(Player.class);
        Game game = mock(Game.class);
        GameManager gameManager = mock(GameManager.class);

        GameController c = new GameController();
        String sId = "1";
        Long id = Long.parseLong(sId);
        mockField(c, gameManager, "gameManager");
        when(request.getParameter("id")).thenReturn(sId);
        when(gameManager.getGameById(id)).thenReturn(game);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("player")).thenReturn(p1);
        when(game.getFirstPlayer()).thenReturn(p1);
        when(game.getSecondPlayer()).thenReturn(null);
        when(request.getRequestDispatcher("/game.jsp")).thenReturn(requestDispatcher);

        c.doGet(request, response);

        verify(request).getParameter("id");
        verify(gameManager).getGameById(id);
        verify(request).setAttribute("id", id);
        verify(request).getSession();
        verify(session).getAttribute("player");
        verify(game, times(3)).getFirstPlayer();
        verify(game, times(3)).getSecondPlayer();
        verify(request).setAttribute("message", "Waiting for an opponent");
        verify(request).setAttribute("opponent", "");
        verify(request).getRequestDispatcher("/game.jsp");
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void GameControllerTestPlayingGame() throws ServletException, IOException {
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        Player p1 = mock(Player.class);
        Player p2 = mock(Player.class);
        Game game = mock(Game.class);
        GameManager gameManager = mock(GameManager.class);

        GameController c = new GameController();
        String sId = "1";
        Long id = Long.parseLong(sId);
        mockField(c, gameManager, "gameManager");
        when(request.getParameter("id")).thenReturn(sId);
        when(gameManager.getGameById(id)).thenReturn(game);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("player")).thenReturn(p1);
        when(game.getFirstPlayer()).thenReturn(p1);
        when(game.getSecondPlayer()).thenReturn(p2);
        when(p1.getLogin()).thenReturn("first");
        when(p2.getLogin()).thenReturn("second");
        when(request.getRequestDispatcher("/game.jsp")).thenReturn(requestDispatcher);

        c.doGet(request, response);

        verify(request).getParameter("id");
        verify(gameManager).getGameById(id);
        verify(request).setAttribute("id", id);
        verify(request).getSession();
        verify(session).getAttribute("player");
        verify(game, times(4)).getFirstPlayer();
        verify(game, times(3)).getSecondPlayer();
        verify(request).setAttribute("message", p1.getLogin() + " vs ");
        verify(request).setAttribute("opponent", p2.getLogin());
        verify(p1, times(2)).getLogin();
        verify(p2, times(2)).getLogin();
        verify(request).getRequestDispatcher("/game.jsp");
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void GameControllerTestWaitingGame() throws ServletException, IOException {
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        Player p1 = mock(Player.class);
        Player p2 = mock(Player.class);
        Game game = mock(Game.class);
        GameManager gameManager = mock(GameManager.class);
        GameController c = new GameController();
        String sId = "1";
        Long id = Long.parseLong(sId);
        mockField(c, gameManager, "gameManager");
        when(request.getParameter("id")).thenReturn(sId);
        when(gameManager.getGameById(id)).thenReturn(game);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("player")).thenReturn(p1);
        when(game.getFirstPlayer()).thenReturn(p2);
        when(game.getSecondPlayer()).thenReturn(null);
        when(p1.getLogin()).thenReturn("first");
        when(p2.getLogin()).thenReturn("second");
        when(request.getRequestDispatcher("/wantJoin")).thenReturn(requestDispatcher);

        c.doGet(request, response);

        verify(request).getParameter("id");
        verify(gameManager).getGameById(id);
        verify(request).setAttribute("id", id);
        verify(request).getSession();
        verify(session).getAttribute("player");
        verify(game, times(5)).getFirstPlayer();
        verify(game, times(2)).getSecondPlayer();
        verify(request).setAttribute("gameOwner", game.getFirstPlayer().getLogin());
        verify(p1, never()).getLogin();
        verify(p2, times(2)).getLogin();
        verify(request).getRequestDispatcher("/wantJoin");
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void GameHistoryControllerTest() throws ServletException, IOException {
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        Player p1 = mock(Player.class);
        Player p2 = mock(Player.class);
        GameHistory gameHistory = mock(GameHistory.class);
        GameManager gameManager = mock(GameManager.class);

        GameHistoryController c = new GameHistoryController();
        String sId = "1";
        Long id = Long.parseLong(sId);
        mockField(c, gameManager, "gameManager");
        when(request.getParameter("id")).thenReturn(sId);
        when(gameManager.getGameHistory(id)).thenReturn(gameHistory);
        when(gameHistory.getFirstPlayer()).thenReturn(p1);
        when(gameHistory.getSecondPlayer()).thenReturn(p2);
        when(p1.getLogin()).thenReturn("first");
        when(p1.getLogin()).thenReturn("second");
        when(p1.getId()).thenReturn(1L);
        when(p2.getId()).thenReturn(2L);
        List<String> GHturns = new ArrayList<>();

        LocalDateTime time = LocalDateTime.now();
        TurnHistory th = new TurnHistory(time, 1L, 2L, "p1");
        GHturns.add(GsonParserService.makeJson(th));

        when(gameHistory.getTurns()).thenReturn(GHturns);
        when(gameHistory.getResultStatus()).thenReturn(ResultStatus.FIRST);
        when(request.getRequestDispatcher("/gameHistory.jsp")).thenReturn(requestDispatcher);

        List<String> turns = new ArrayList<>();
        turns.add(String.format("turn: %s\t player: %s\t date: %s",
                th.getTurn(), p1.getLogin(), th.getTime().format(DateTimeFormatter.ISO_DATE_TIME)));

        c.doGet(request, response);

        verify(request).getParameter("id");
        verify(gameManager).getGameHistory(id);
        verify(gameHistory, times(1)).getFirstPlayer();
        verify(gameHistory, times(1)).getSecondPlayer();
        verify(gameHistory, times(1)).getTurns();
        verify(p1, times(1)).getId();
        verify(p1, times(4)).getLogin();
        verify(p2, times(1)).getLogin();
        verify(request).setAttribute("id", gameHistory.getGameId());
        verify(request).setAttribute("firstPlayer", p1.getLogin());
        verify(request).setAttribute("secondPlayer", p2.getLogin());
        verify(request).setAttribute("winner", p1.getLogin());
        verify(request).setAttribute("turns", turns);
        verify(request).getRequestDispatcher("/gameHistory.jsp");
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void GameListControllerTest() throws ServletException, IOException {
        GameListController c = new GameListController();
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        Player player = mock(Player.class);
        when(player.getLogin()).thenReturn("first");
        GameManager gameManager = mock(GameManager.class);
        mockField(c, gameManager, "gameManager");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("player")).thenReturn(player);
        List<Long> ids = new ArrayList<>();
        when(gameManager.getCreatedGameIDs(player)).thenReturn(ids);
        when(gameManager.getPlayingGameIDs(player)).thenReturn(ids);
        when(gameManager.getWaitingGameIDs(player)).thenReturn(ids);
        when(request.getRequestDispatcher("/gameList.jsp")).thenReturn(requestDispatcher);

        c.doGet(request, response);

        verify(request).getSession();
        verify(session).getAttribute("player");
        verify(player).getLogin();
        verify(gameManager).getCreatedGameIDs(player);
        verify(gameManager).getPlayingGameIDs(player);
        verify(gameManager).getWaitingGameIDs(player);
        verify(request).setAttribute("playingGames", ids);
        verify(request).setAttribute("createdGames", ids);
        verify(request).setAttribute("waitingGames", ids);
        verify(request).getRequestDispatcher("/gameList.jsp");
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void LoginControllerDoGetMethod() throws ServletException, IOException {
        LoginController c = new LoginController();
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        when(request.getRequestDispatcher("/login.jsp")).thenReturn(requestDispatcher);

        c.doGet(request, response);

        verify(request).getRequestDispatcher("/login.jsp");
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void LoginControllerValidPlayerTest() throws ServletException, IOException {
        LoginController c = new LoginController();
        String login = "first";
        String password = "111";
        when(request.getParameter("login")).thenReturn(login);
        when(request.getParameter("password")).thenReturn(password);
        PlayerManager playerManager = mock(PlayerManager.class);
        mockField(c, playerManager, "playerManager");
        Player player = mock(Player.class);
        when(playerManager.getPlayerByLogin(login)).thenReturn(player);
        when(player.getPassword()).thenReturn(password);
        PasswordEncryptionService pes = mock(PasswordEncryptionService.class);
        mockField(c, pes, "pes");

        when(pes.checkPassword(password, player.getPassword())).thenReturn(true);
        when(request.getSession()).thenReturn(session);

        c.doPost(request, response);

        verify(request).getParameter("login");
        verify(request).getParameter("password");
        verify(playerManager).getPlayerByLogin(login);
        verify(player, times(2)).getPassword();
        verify(pes).checkPassword(password, password);
        verify(session).setAttribute("player", player);
        verify(response).sendRedirect("gameList");

    }

    @Test
    public void LoginControllerNotValidPlayerTest() throws ServletException, IOException {
        LoginController c = new LoginController();
        String login = "first";
        String password = "111";
        when(request.getParameter("login")).thenReturn(login);
        when(request.getParameter("password")).thenReturn(password);
        PlayerManager playerManager = mock(PlayerManager.class);
        mockField(c, playerManager, "playerManager");
        Player player = mock(Player.class);
        when(playerManager.getPlayerByLogin(login)).thenReturn(player);
        when(player.getPassword()).thenReturn(password);
        PasswordEncryptionService pes = mock(PasswordEncryptionService.class);
        mockField(c, pes, "pes");
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        when(request.getRequestDispatcher("/login.jsp")).thenReturn(requestDispatcher);

        when(pes.checkPassword(password, player.getPassword())).thenReturn(false);

        c.doPost(request, response);

        verify(request).getParameter("login");
        verify(request).getParameter("password");
        verify(playerManager).getPlayerByLogin(login);
        verify(player, times(2)).getPassword();
        verify(pes).checkPassword(password, password);
        verify(request).setAttribute("login", login);
        verify(request).setAttribute("message", "Wrong login or password");
        verify(request).getRequestDispatcher("/login.jsp");
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void RegisterControllerTestDoGet() throws ServletException, IOException {
        RegisterController c = new RegisterController();
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        when(request.getRequestDispatcher("/registration.jsp")).thenReturn(requestDispatcher);

        c.doGet(request, response);

        verify(request).getRequestDispatcher("/registration.jsp");
        verify(requestDispatcher).forward(request, response);
    }

    @Test
    public void RegisterControllerTestDoPost() throws ServletException, IOException {
        RegisterController c = new RegisterController();
        String login = "first";
        String password = "111";
        String email = "email";
        String fullName = "fullName";
        String gender = "MALE";
        String date = "2016-09-12";
        String about = "about";

        when(request.getParameter("login")).thenReturn(login);
        when(request.getParameter("password")).thenReturn(password);
        when(request.getParameter("email")).thenReturn(email);
        when(request.getParameter("fullName")).thenReturn(fullName);
        when(request.getParameter("gender")).thenReturn(gender);
        when(request.getParameter("date")).thenReturn(date);
        when(request.getParameter("about")).thenReturn(about);

        PlayerManager playerManager = mock(PlayerManager.class);
        mockField(c, playerManager, "playerManager");
        Player player = mock(Player.class);
        when(playerManager.getPlayerByLogin(login)).thenReturn(player);
        when(request.getSession()).thenReturn(session);

        c.doPost(request, response);

        verify(request).getParameter("login");
        verify(request).getParameter("password");
        verify(request).getParameter("email");
        verify(request).getParameter("fullName");
        verify(request).getParameter("gender");
        verify(request).getParameter("date");
        verify(request).getParameter("about");
        verify(session).setAttribute("player", player);
        verify(response).sendRedirect("gameList");
    }

    @Test
    public void RegisterControllerTestDoPostWithDefaultParam() throws ServletException, IOException {
        RegisterController c = new RegisterController();
        String login = "first";
        String password = "111";
        String email = "email";
        String fullName = "";
        String gender = null;
        String date = "";
        String about = "";

        when(request.getParameter("login")).thenReturn(login);
        when(request.getParameter("password")).thenReturn(password);
        when(request.getParameter("email")).thenReturn(email);
        when(request.getParameter("fullName")).thenReturn(fullName);
        when(request.getParameter("gender")).thenReturn(gender);
        when(request.getParameter("date")).thenReturn(date);
        when(request.getParameter("about")).thenReturn(about);

        PlayerManager playerManager = mock(PlayerManager.class);
        mockField(c, playerManager, "playerManager");
        Player player = mock(Player.class);
        when(playerManager.getPlayerByLogin(login)).thenReturn(player);
        when(request.getSession()).thenReturn(session);

        c.doPost(request, response);

        verify(request).getParameter("login");
        verify(request).getParameter("password");
        verify(request).getParameter("email");
        verify(request).getParameter("fullName");
        verify(request).getParameter("gender");
        verify(request).getParameter("date");
        verify(request).getParameter("about");
        verify(session).setAttribute("player", player);
        verify(response).sendRedirect("gameList");
    }


    @Test
    public void UpdateControllerTest() throws ServletException, IOException {
        UpdateController c = new UpdateController();
        String login = "first";
        String email = "email";
        String fullName = "fullName";
        String gender = "MALE";
        String date = "2016-09-12";
        String about = "about";

        when(request.getSession()).thenReturn(session);
        Player player = mock(Player.class);
        when(player.getLogin()).thenReturn(login);
        when(session.getAttribute("player")).thenReturn(player);

        when(request.getParameter("email")).thenReturn(email);
        when(request.getParameter("fullName")).thenReturn(fullName);
        when(request.getParameter("gender")).thenReturn(gender);
        when(request.getParameter("date")).thenReturn(date);
        when(request.getParameter("about")).thenReturn(about);

        PlayerManager playerManager = mock(PlayerManager.class);
        mockField(c, playerManager, "playerManager");
        when(playerManager.getPlayerByLogin(login)).thenReturn(player);

        c.doPost(request, response);

        verify(request).getParameter("email");
        verify(request).getParameter("fullName");
        verify(request).getParameter("gender");
        verify(request).getParameter("date");
        verify(request).getParameter("about");
        verify(playerManager).updatePlayer(player);
        verify(playerManager).getPlayerByLogin(login);
        verify(player, times(3)).getLogin();
        verify(response).sendRedirect("profile?login="+login);
    }


    @Test
    public void UpdateControllerTestWithDefaultParam() throws ServletException, IOException {
        UpdateController c = new UpdateController();
        String login = "first";
        String email = "";
        String fullName = "";
        String gender = null;
        String date = "";
        String about = "";

        when(request.getSession()).thenReturn(session);
        Player player = mock(Player.class);
        when(player.getLogin()).thenReturn(login);
        when(session.getAttribute("player")).thenReturn(player);

        when(request.getParameter("email")).thenReturn(email);
        when(request.getParameter("fullName")).thenReturn(fullName);
        when(request.getParameter("gender")).thenReturn(gender);
        when(request.getParameter("date")).thenReturn(date);
        when(request.getParameter("about")).thenReturn(about);

        PlayerManager playerManager = mock(PlayerManager.class);
        mockField(c, playerManager, "playerManager");
        when(playerManager.getPlayerByLogin(login)).thenReturn(player);

        c.doPost(request, response);

        verify(request).getParameter("email");
        verify(request).getParameter("fullName");
        verify(request).getParameter("gender");
        verify(request).getParameter("date");
        verify(request).getParameter("about");
        verify(playerManager).updatePlayer(player);
        verify(playerManager).getPlayerByLogin(login);
        verify(player, times(3)).getLogin();
        verify(response).sendRedirect("profile?login="+login);
    }


    @Test
    public void  ProfileControllerTest() throws ServletException, IOException {
        ProfileController c = new ProfileController();
        RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
        Player p1 = mock(Player.class);
        Player p2 = mock(Player.class);

        PlayerManager playerManager = mock(PlayerManager.class);
        mockField(c, playerManager, "playerManager");

        String login = "first";
        when(request.getParameter("login")).thenReturn(login);
        when(playerManager.getPlayerByLogin(login)).thenReturn(p1);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("player")).thenReturn(p1);
        when(request.getRequestDispatcher("/myProfile.jsp")).thenReturn(requestDispatcher);

        c.doGet(request, response);

        verify(request).getParameter("login");
        verify(playerManager).getPlayerByLogin(login);
        verify(request).getSession();
        verify(session).getAttribute("player");
        verify(request).setAttribute("player", p1);
        verify(request).getRequestDispatcher("/myProfile.jsp");
        verify(requestDispatcher).forward(request, response);
    }






    private void mockField(HttpServlet controller, Object o, String field){
        try {
            Field field1 = controller.getClass().getDeclaredField(field);
            field1.setAccessible(true);
            field1.set(controller, o);
        } catch (Exception e) {
            fail("Field '" + field + "' is not accessible");
        }
    }

}
