package com.softserveinc.ita.multigame.dao;

import com.softserveinc.ita.multigame.dao.impl.GameHistoryDaoImpl;
import com.softserveinc.ita.multigame.dao.impl.HSQLTestEntityManagerFactoryImpl;
import com.softserveinc.ita.multigame.dao.impl.PlayerDaoImpl;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.ResultStatus;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class GameHistoryDaoImplTest {

    private GameHistoryDao dao = new GameHistoryDaoImpl();
    private PlayerDao playerDao = new PlayerDaoImpl();
    private MyEntityManagerFactory emf = HSQLTestEntityManagerFactoryImpl.getInstance();
    private MyEntityManagerFactory emfMock = mock(MyEntityManagerFactory.class);



    @Before
    public void setUp(){
        try {
            Field field1 = dao.getClass().getDeclaredField("emf");
            field1.setAccessible(true);
            field1.set(dao, emfMock);
            Field field2 = playerDao.getClass().getDeclaredField("emf");
            field2.setAccessible(true);
            field2.set(playerDao, emfMock);
        } catch (Exception e) {
            fail("Field \"emf\" is not accessible");
        }
    }

    @Test
    public void GameHistoryCouldBeGetByIdFromDao() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertSame(1L, dao.getById(1L).getGameId());
        verify(emfMock).getEntityManager();
    }

    @Test
    public void NullCouldNotBeGetByIdFromDao() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertNull(dao.getById(null));
        verify(emfMock, never()).getEntityManager();
    }

    @Test
    public void PlayerGameHistoriesCouldBeGetFromTheDao() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p1 = playerDao.getById(1L);
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p2 = playerDao.getById(2L);

        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        List<GameHistory> gameHistories1 = dao.getAllGames(p1);
        assertEquals(1, gameHistories1.size());

        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        List<GameHistory> gameHistories2 = dao.getAllGames(p2);

        assertSame(1, gameHistories2.size());

        assertEquals(gameHistories1, gameHistories2);

        verify(emfMock, times(4)).getEntityManager();
    }

    @Test
    public void PlayerGameHistoriesCouldNotBeGetFromTheDaoIfPlayerNull() {
        List<GameHistory> gameHistories = dao.getAllGames(null);
        assertNull(gameHistories);

    }

    @Test
    public void GameHistoryCouldBeSaved() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p1 = playerDao.getById(3L);
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p2 = playerDao.getById(4L);
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        GameHistory gh1 = new GameHistory(110L, p1, p2, LocalDateTime.now(), LocalDateTime.now(), new ArrayList<>(), ResultStatus.DRAWN);
        assertTrue(dao.save(gh1));
        verify(emfMock, times(3)).getEntityManager();
    }

    @Test
    public void nullCouldNotBeSavedTwice() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        GameHistory gh = new GameHistory(1L, null, null, null, null, null, ResultStatus.DRAWN);
        assertFalse(dao.save(gh));
        verify(emfMock).getEntityManager();
    }

    @Test
    public void nullCouldNotBeSaved() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertFalse(dao.save(null));
        verify(emfMock, never()).getEntityManager();
    }

    @Test
    public void GameHistoryCouldBeDeleted() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p1 = playerDao.getById(1L);
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p2 = playerDao.getById(2L);
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        GameHistory gh1 = new GameHistory(120L, p1, p2, LocalDateTime.now(), LocalDateTime.now(), new ArrayList<>(), ResultStatus.DRAWN);
        assertTrue(dao.save(gh1));
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertTrue(dao.delete(gh1));
        verify(emfMock, times(4)).getEntityManager();

    }

    @Test
    public void nullCouldNotBeDeleted() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertFalse(dao.delete(null));
        verify(emfMock).getEntityManager();
    }

}
