package com.softserveinc.ita.multigame.dao;

import com.softserveinc.ita.multigame.dao.impl.HSQLTestEntityManagerFactoryImpl;
import com.softserveinc.ita.multigame.dao.impl.PlayerDaoImpl;
import com.softserveinc.ita.multigame.model.Player;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PlayerDaoImplTest {
    private PlayerDao dao = new PlayerDaoImpl();
    private MyEntityManagerFactory emf = HSQLTestEntityManagerFactoryImpl.getInstance();
    private MyEntityManagerFactory emfMock = mock(MyEntityManagerFactory.class);

    @Before
    public void setUp() {
        try {
            Field field = dao.getClass().getDeclaredField("emf");
            field.setAccessible(true);
            field.set(dao, emfMock);
        } catch (Exception e) {
            fail("Field \"emf\" is not accessible");
        }
    }

    @Test
    public void PlayerCouldBeGetByIdFromTheDao() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p1 = dao.getById(1L);
        assertSame(1L, p1.getId());
        verify(emfMock).getEntityManager();
    }

    @Test
    public void AllPlayersCouldBeGetFromTheDao() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p1 = dao.getById(1L);
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        List<Player> players = dao.getAll();
        assertTrue(players.contains(p1));
        verify(emfMock, times(2)).getEntityManager();

    }

    @Test
    public void NullCouldNotBeGetByIdFromTheDao() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertNull(dao.getById(null));
        verify(emfMock, never()).getEntityManager();
    }

    @Test
    public void PlayerCouldNotBeGetByIdFromTheDaoIfNotCreate() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertNull(dao.getById(101L));
        verify(emfMock).getEntityManager();
    }

    @Test
    public void PlayerCouldBeGetByLoginFromTheDao() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p = dao.getById(1L);
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p1 = dao.getByLogin(p.getLogin());
        assertEquals(p, p1);
        verify(emfMock, times(2)).getEntityManager();

    }

    @Test
    public void NullCouldNotBeGetByLoginFromTheDao() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertNull(dao.getByLogin(null));
        verify(emfMock, never()).getEntityManager();
    }

    @Test
    public void playerCouldNotBeSavedTwice() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p = dao.getById(1L);

        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertFalse(dao.save(p));
        verify(emfMock, times(2)).getEntityManager();
    }

    @Test
    public void nullCouldNotBeSaved() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertFalse(dao.save(null));
        verify(emfMock, never()).getEntityManager();
    }

    @Test
    public void playerCouldBeUpdateIfItCreated() throws SQLException {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        String fullName = "FullName";
        Player p = dao.getById(1L);
        p.setFullName(fullName);

        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertTrue(dao.update(p));

        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        Player p1 = dao.getById(1L);
        assertEquals(fullName, p1.getFullName());
        assertEquals(p, p1);
        verify(emfMock, times(3)).getEntityManager();
    }


    @Test
    public void PlayerCouldNotBeUpdateByIdFromTheDaoIfNotCreate() {
        Player p = new Player("111", "111", "111");
        p.setId(22L);
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertFalse(dao.update(p));
        verify(emfMock).getEntityManager();
    }

    @Test
    public void nullCouldNotBeUpdated() throws SQLException {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertFalse(dao.update(null));
        verify(emfMock).getEntityManager();
    }

    @Test
    public void playerCouldBeDeletedIfItCreated() {
        Player p = new Player("first", "111", "email");

        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        dao.save(p);

        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertTrue(dao.delete(p));

        verify(emfMock, times(2)).getEntityManager();
    }

    @Test
    public void PlayerCouldNotBeDeletedByIdFromTheDaoIfNotCreate() {
        Player p = new Player("111", "111", "111");
        p.setId(3L);
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertFalse(dao.delete(p));
        verify(emfMock).getEntityManager();
    }

    @Test
    public void nullCouldNotBeDeleted() {
        when(emfMock.getEntityManager()).thenReturn(emf.getEntityManager());
        assertFalse(dao.delete(null));
        verify(emfMock).getEntityManager();
    }

}
