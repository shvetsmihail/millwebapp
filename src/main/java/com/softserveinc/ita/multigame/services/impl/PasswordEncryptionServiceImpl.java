package com.softserveinc.ita.multigame.services.impl;


import com.softserveinc.ita.multigame.services.PasswordEncryptionService;
import org.jasypt.util.password.StrongPasswordEncryptor;

public class PasswordEncryptionServiceImpl implements PasswordEncryptionService {
    private StrongPasswordEncryptor passwordEncryptor;

    private PasswordEncryptionServiceImpl() {
        passwordEncryptor = new StrongPasswordEncryptor();
    }

    private static class SingletonHolder {
        static final PasswordEncryptionServiceImpl HOLDER_INSTANCE = new PasswordEncryptionServiceImpl();
    }

    public static PasswordEncryptionServiceImpl getInstance() {
        return PasswordEncryptionServiceImpl.SingletonHolder.HOLDER_INSTANCE;
    }

    @Override
    public String encryptPassword(String password) {
        return passwordEncryptor.encryptPassword(password);
    }

    @Override
    public boolean checkPassword(String originalPassword, String encryptedPasswordStrong) {
        return passwordEncryptor.checkPassword(originalPassword, encryptedPasswordStrong);
    }
}
