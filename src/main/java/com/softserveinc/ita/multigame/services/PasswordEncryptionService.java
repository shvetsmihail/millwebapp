package com.softserveinc.ita.multigame.services;

public interface PasswordEncryptionService {
    String encryptPassword(String originalPassword);
    boolean checkPassword(String originalPassword, String encryptedPasswordStrong);
}
