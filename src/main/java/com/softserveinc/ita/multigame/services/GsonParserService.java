package com.softserveinc.ita.multigame.services;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonParserService {
    public static String makeJson(Object object) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(object);
    }

    public static Object fromJson(String json, Class clazz) {
        Gson gson = new Gson();

        return gson.fromJson(json, clazz);
    }
}
