package com.softserveinc.ita.multigame.model.engine.millEngine;

import java.util.List;

class BoardDTO {
    private List<String> stonesOnBoard;
    private int firstPlayerCountOfStoneInHand;
    private int secondPlayerCountOfStoneInHand;

    BoardDTO(List<String> stonesOnBoard,
                    int firstPlayerCountOfStoneInHand,
                    int secondPlayerCountOfStoneInHand) {
        this.stonesOnBoard = stonesOnBoard;
        this.firstPlayerCountOfStoneInHand = firstPlayerCountOfStoneInHand;
        this.secondPlayerCountOfStoneInHand = secondPlayerCountOfStoneInHand;
    }

    public List<String> getStonesOnBoard() {
        return stonesOnBoard;
    }

    public int getFirstPlayerCountOfStoneInHand() {
        return firstPlayerCountOfStoneInHand;
    }

    public int getSecondPlayerCountOfStoneInHand() {
        return secondPlayerCountOfStoneInHand;
    }
}
