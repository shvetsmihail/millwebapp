package com.softserveinc.ita.multigame.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class GameHistory implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Long gameId;
    @ManyToOne
    @JoinColumn(name = "first_player_id", nullable = true, updatable = false)
    private Player firstPlayer;
    @ManyToOne
    @JoinColumn(name = "second_player_id", nullable = true, updatable = false)
    private Player secondPlayer;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    @ElementCollection(fetch = FetchType.LAZY)
    private List<String> turns;
    @Enumerated(EnumType.STRING)
    private ResultStatus resultStatus;

    public GameHistory(Long gameId, Player firstPlayer, Player secondPlayer,
                       LocalDateTime startTime, LocalDateTime endTime,
                       List<String> turns, ResultStatus resultStatus) {
        this.gameId = gameId;
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.startTime = startTime;
        this.endTime = endTime;
        this.turns = turns;
        this.resultStatus = resultStatus;
    }

    public GameHistory() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameHistory that = (GameHistory) o;

        return gameId != null ? gameId.equals(that.gameId) : that.gameId == null;

    }

    @Override
    public int hashCode() {
        return gameId != null ? gameId.hashCode() : 0;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public Player getFirstPlayer() {
        return firstPlayer;
    }

    public void setFirstPlayer(Player firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    public Player getSecondPlayer() {
        return secondPlayer;
    }

    public void setSecondPlayer(Player secondPlayer) {
        this.secondPlayer = secondPlayer;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public List<String> getTurns() {
        return turns;
    }

    public void setTurns(List<String> turns) {
        this.turns = turns;
    }

    public ResultStatus getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(ResultStatus resultStatus) {
        this.resultStatus = resultStatus;
    }
}
