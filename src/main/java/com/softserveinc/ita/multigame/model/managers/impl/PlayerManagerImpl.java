package com.softserveinc.ita.multigame.model.managers.impl;


import com.softserveinc.ita.multigame.dao.PlayerDao;
import com.softserveinc.ita.multigame.dao.impl.PlayerDaoImpl;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.managers.PlayerManager;

import java.util.List;

public class PlayerManagerImpl implements PlayerManager{
    private PlayerDao dao = new PlayerDaoImpl();

    private PlayerManagerImpl(){
    }

    private static class SingletonHolder {
        static final PlayerManagerImpl HOLDER_INSTANCE = new PlayerManagerImpl();
    }

    public static PlayerManagerImpl getInstance() {
        return PlayerManagerImpl.SingletonHolder.HOLDER_INSTANCE;
    }

    @Override
    public boolean registerNewPlayer(Player player) {
        return dao.save(player);
    }

    @Override
    public Player getPlayerByLogin(String login) {
        return dao.getByLogin(login);
    }

    @Override
    public List<Player> getAll() {
        return dao.getAll();
    }

    @Override
    public boolean updatePlayer(Player player) {
        return dao.update(player);
    }

    @Override
    public boolean deletePlayer(Player player) {
        return GameManagerImpl.getInstance().deleteGameHistory(player)
                && dao.delete(player);
    }
}
