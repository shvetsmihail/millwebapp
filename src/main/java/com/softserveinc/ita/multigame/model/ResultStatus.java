package com.softserveinc.ita.multigame.model;


public enum ResultStatus {
    FIRST, SECOND, DRAWN, NONE
}
