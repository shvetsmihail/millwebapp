package com.softserveinc.ita.multigame.model.managers;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;

import java.util.List;

public interface GameManager {
    boolean createGame(Player player);
    boolean deleteGame(Long id);
    Game getGameById(Long id);
    List<Long> getCreatedGameIDs(Player player);
    List<Long> getPlayingGameIDs(Player player);
    List<Long> getWaitingGameIDs(Player player);
    List<Long> getAllIDs();
    List<GameHistory> getGameHistories(Player player);
    boolean deleteGameHistory(Player player);
    GameHistory getGameHistory(Long gameId);

}
