package com.softserveinc.ita.multigame.model;


import java.time.LocalDateTime;

public class TurnHistory {
    private LocalDateTime time;
    private Long playerId;
    private Long gameId;
    private String turn;

    public TurnHistory(LocalDateTime time, Long playerId, Long gameId, String turn) {
        this.time = time;
        this.playerId = playerId;
        this.gameId = gameId;
        this.turn = turn;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public Long getGameId() {
        return gameId;
    }

    public String getTurn() {
        return turn;
    }
}
