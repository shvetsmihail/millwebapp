package com.softserveinc.ita.multigame.model.managers.impl;

import com.softserveinc.ita.multigame.dao.GameHistoryDao;
import com.softserveinc.ita.multigame.dao.impl.GameHistoryDaoImpl;
import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.managers.GameManager;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class GameManagerImpl implements GameManager {
    private Map<Long, Game> games = new ConcurrentHashMap<>();
    private Map<Long, Game> deletedGames = new ConcurrentHashMap<>();
    private GameHistoryDao gameHistoryDao = new GameHistoryDaoImpl();

    private GameManagerImpl() {
    }

    private static class SingletonHolder {
        static final GameManagerImpl HOLDER_INSTANCE = new GameManagerImpl();
    }

    public static GameManagerImpl getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    @Override
    public boolean createGame(Player player) {
        if (player != null) {
            Game game = new Game(player);
            games.put(game.getId(), game);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteGame(Long id) {
        if (id == null) return false;
        Game game  = games.remove(id);
        if (game != null){
            game.setEndTime(LocalDateTime.now());
            saveGameHistory(game);
            deletedGames.put(game.getId(), game);

            return true;
        }
        return false;
    }

    private void saveGameHistory(Game game) {
        GameHistory gh = new GameHistory(game.getId(), game.getFirstPlayer(), game.getSecondPlayer(),
                game.getStartTime(), game.getEndTime(), game.getTurns(), game.getResultStatus());
        gameHistoryDao.save(gh);
    }

    @Override
    public Game getGameById(Long id) {
        if (games.containsKey(id)){
            Game game = games.get(id);
            checkGameIsFinished(game);
            return game;
        }

        if (deletedGames.containsKey(id)){
            return deletedGames.get(id);
        }
        return null;
    }

    private void checkGameIsFinished(Game game) {
        if (game.isFinished()){
            deleteGame(game.getId());
        }
    }

    @Override
    public List<Long> getCreatedGameIDs(Player player) {
        List<Long> ids = new ArrayList<>();

        for (Long id : games.keySet()) {

            Game game = games.get(id);

            if (player.equals(game.getFirstPlayer())
                    && game.getSecondPlayer() == null) {
                ids.add(id);
            }
        }
        return ids;
    }


    @Override
    public List<Long> getPlayingGameIDs(Player player) {
        List<Long> ids = new ArrayList<>();

        for (Long id : games.keySet()) {
            Game game = games.get(id);
            if ((player.equals(game.getFirstPlayer())
                    && game.getSecondPlayer() != null)
                    || player.equals(game.getSecondPlayer())) {
                ids.add(id);
            }
        }
        return ids;
    }

    @Override
    public List<Long> getWaitingGameIDs(Player player) {
        List<Long> ids = new ArrayList<>();

        for (Long id : games.keySet()) {
            Game game = games.get(id);
            if (!player.equals(game.getFirstPlayer())
                    && !player.equals(game.getSecondPlayer())
                    && game.getFirstPlayer() != null) {

                ids.add(id);
            }
        }
        return ids;
    }

    @Override
    public List<Long> getAllIDs() {
        return new ArrayList<>(games.keySet());
    }

    @Override
    public List<GameHistory> getGameHistories(Player player) {
        return gameHistoryDao.getAllGames(player);
    }

    @Override
    public boolean deleteGameHistory(Player player) {
        List<GameHistory> gameHistories = GameManagerImpl.getInstance().getGameHistories(player);

        if (gameHistories == null || gameHistories.isEmpty()) return false;

        boolean isAllDeleted = true;

        for (GameHistory gh : gameHistories){
            isAllDeleted = isAllDeleted && gameHistoryDao.delete(gh);
        }

        return isAllDeleted;
    }

    @Override
    public GameHistory getGameHistory(Long gameId) {
        return gameHistoryDao.getById(gameId);
    }

}
