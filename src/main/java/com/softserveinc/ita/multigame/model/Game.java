package com.softserveinc.ita.multigame.model;


import com.softserveinc.ita.multigame.model.engine.GameResultCode;
import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.model.engine.millEngine.Commands;
import com.softserveinc.ita.multigame.model.engine.millEngine.MillGameEngine;
import com.softserveinc.ita.multigame.services.GsonParserService;
import org.apache.log4j.Logger;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class Game {
    private MillGameEngine game;
    private Player firstPlayer;
    private Player secondPlayer;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private List<String> turns;
    private Logger logger = Logger.getLogger(Game.class);

    public Game(Player player) {
        game = new MillGameEngine();
        game.setFirstPlayer(player.getLogin());
        firstPlayer = player;;
        startTime = LocalDateTime.now();
        turns = new ArrayList<>();
    }

    //only for test
    void setGame(MillGameEngine game) {
        this.game = game;
    }

    public Long getId(){
        return game.getId();
    }

    public boolean joinToGame(Player player){
        if (game.getSecondPlayer() == null){
            game.setSecondPlayer(player.getLogin());
            secondPlayer = player;
            return true;
        }
        return false;
    }

    public List<String> getTurns() {
        return turns;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Player getFirstPlayer() {
        return firstPlayer;
    }

    public Player getSecondPlayer() {
        return secondPlayer;
    }

    public boolean makeTurn(Player player, String turn){
        saveTurn(player, turn);
        return game.makeTurn(player.getLogin(), turn);
    }

    public boolean makeTurn(Player player, String position1, String position2){
        String turn;
        if (position2 != null){
            turn = Commands.REPLACE_STONE + " " + position1 + " " + position2;
        } else if (game.getGameState() == GameState.WAIT_FOR_FIRST_PLAYER_TURN || game.getGameState() == GameState.WAIT_FOR_SECOND_PLAYER_TURN  ){
            turn = Commands.PUT_NEW_STONE + " " + position1;
        } else {
            turn = Commands.DROP_STONE + " " + position1;
        }
        logger.debug(player.getLogin() + " made turn: " + turn);
        saveTurn(player, turn);
        return game.makeTurn(player.getLogin(), turn);
    }


    private void saveTurn(Player player, String turn) {
        TurnHistory turnHist = new TurnHistory(LocalDateTime.now(), player.getId(), game.getId(), turn);
        String gSonTurn = GsonParserService.makeJson(turnHist);
        turns.add(gSonTurn);
    }

    public String getBoard(){
        return game.getBoard();
    }

    public String getTips(Player player){
        String message = ">" + getColor(player) + ": ";
        if (game.isFinished()){
            message += "Game Over. Congratulation " + game.getTheWinner();
        } else if (game.isStarted()) {
            if (game.isCurrentPlayer(player.getLogin())) {
                switch (game.getResultCode()) {
                    case GameResultCode.BAD_TURN_LOGIC:
                        message += "Wrong command. Read rules and try again!";
                        break;
                    case GameResultCode.BAD_TURN_SYNTAX:
                        message += "Mistake in command. Please try again!";
                        break;
                    default:
                        message += "Your turn ";
                }
            } else {
                message += "Opponents turn";
            }
        } else {
            message += "Please wait for your opponent";
        }
        return message;
    }

    public String getColor(Player player){
        if (player.equals(getFirstPlayer())){
            return "White";
        } else {
            return "Black";
        }
    }

    public String getStonesInHand(){
        return GsonParserService.makeJson(game.getStonesList());
    }

    public boolean isFinished(){
        return game.isFinished();
    }

    public ResultStatus getResultStatus() {

        if (game.getTheWinner().equals(firstPlayer.getLogin())){
            return ResultStatus.FIRST;
        }
        if (game.getTheWinner().equals(secondPlayer.getLogin())){
            return ResultStatus.SECOND;
        }
        return ResultStatus.NONE;
    }
}
