package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.managers.PlayerManager;
import com.softserveinc.ita.multigame.model.managers.impl.PlayerManagerImpl;
import com.softserveinc.ita.multigame.services.PasswordEncryptionService;
import com.softserveinc.ita.multigame.services.impl.PasswordEncryptionServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private PlayerManager playerManager = PlayerManagerImpl.getInstance();
    private PasswordEncryptionService pes = PasswordEncryptionServiceImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Logger logger = Logger.getLogger(LoginController.class);

        String login = req.getParameter("login");
        String password = req.getParameter("password");

        Player player = playerManager.getPlayerByLogin(login);

        if (player != null && pes.checkPassword(password, player.getPassword())) {
            logger.info(String.format("player %s entered to game", login));
            req.getSession().setAttribute("player", player);
            resp.sendRedirect("gameList");
        } else {
            logger.info(String.format("player %s tried to enter to game", login));
            req.setAttribute("login", login);
            req.setAttribute("message", "Wrong login or password");
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        }
    }
}

