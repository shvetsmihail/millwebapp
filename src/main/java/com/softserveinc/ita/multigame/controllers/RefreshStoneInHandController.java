package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.managers.GameManager;
import com.softserveinc.ita.multigame.model.managers.impl.GameManagerImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/refreshStones/*")
public class RefreshStoneInHandController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GameManager gameManager = GameManagerImpl.getInstance();

        Long id = (Long) Long.parseLong(req.getParameter("id"));
        Game game = gameManager.getGameById(id);
        String stonesInHand = game.getStonesInHand();
        resp.getWriter().print(stonesInHand);
    }
}
