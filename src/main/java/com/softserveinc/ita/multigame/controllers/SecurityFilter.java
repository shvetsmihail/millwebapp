package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.services.impl.CreateUsersAndGamesService;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class SecurityFilter implements Filter {
    private static final long serialVersionUID = 1L;
    private Logger logger = Logger.getLogger(this.getClass());

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        CreateUsersAndGamesService.create();

        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        HttpServletRequest req = (HttpServletRequest) servletRequest;

        String servletPath = req.getServletPath();

        if (servletPath.contains(".css") || servletPath.contains(".png")) {
            logger.debug("doFilter() ignore css and png");
            filterChain.doFilter(req, resp);
            return;
        }

        if (servletPath.equals("/login")) {
            logger.debug("doFilter() ignore login");
            filterChain.doFilter(req, resp);
            return;
        }

        if (servletPath.equals("/registration")) {
            logger.debug("doFilter() ignore login");
            filterChain.doFilter(req, resp);
            return;
        }

        Player player = (Player) req.getSession().getAttribute("player");
        if (player != null) {
            filterChain.doFilter(req, resp);
            return;
        }

        String url = req.getContextPath() + "/login";
        resp.sendRedirect(url);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {

    }
}
