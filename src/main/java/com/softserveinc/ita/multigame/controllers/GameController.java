package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.managers.GameManager;
import com.softserveinc.ita.multigame.model.managers.impl.GameManagerImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/game/*")
public class GameController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private GameManager gameManager = GameManagerImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Logger logger = Logger.getLogger(GameController.class);

        Long id = Long.parseLong(req.getParameter("id"));
        Game game = gameManager.getGameById(id);

        req.setAttribute("id", id);

        Player player = (Player) req.getSession().getAttribute("player");

        //creating game
        if (player.equals(game.getFirstPlayer()) && game.getSecondPlayer() == null) {
            req.setAttribute("message", "Waiting for an opponent");
            req.setAttribute("opponent", "");
            req.getRequestDispatcher("/game.jsp").forward(req, resp);
        }

        //playing game
        if ((player.equals(game.getFirstPlayer()) && game.getSecondPlayer() != null)
                || player.equals(game.getSecondPlayer())) {

            Player opponent;

            if (player.equals(game.getFirstPlayer())){
                opponent = game.getSecondPlayer();
            } else {
                opponent = game.getFirstPlayer();
            }

            req.setAttribute("message", player.getLogin() + " vs ");
            req.setAttribute("opponent", opponent.getLogin());
            req.getRequestDispatcher("/game.jsp").forward(req, resp);
        }

        //waiting game
        if (!player.equals(game.getFirstPlayer())
                && !player.equals(game.getSecondPlayer())
                && game.getFirstPlayer() != null) {
            req.setAttribute("gameOwner", game.getFirstPlayer().getLogin());
            req.getRequestDispatcher("/wantJoin").forward(req, resp);
        }
    }

}
