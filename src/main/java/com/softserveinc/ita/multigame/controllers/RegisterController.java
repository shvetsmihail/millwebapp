package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Gender;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.managers.PlayerManager;
import com.softserveinc.ita.multigame.model.managers.impl.PlayerManagerImpl;
import com.softserveinc.ita.multigame.services.PasswordEncryptionService;
import com.softserveinc.ita.multigame.services.impl.PasswordEncryptionServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;

@WebServlet("/registration")
public class RegisterController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private PasswordEncryptionService pes = PasswordEncryptionServiceImpl.getInstance();
    private PlayerManager playerManager = PlayerManagerImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/registration.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Logger logger = Logger.getLogger(RegisterController.class);

        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String email = req.getParameter("email");

        Player player = new Player(login, pes.encryptPassword(password), email);

        String fullName = req.getParameter("fullName");
        if (!fullName.equals("")) {
            player.setFullName(fullName);
        }

        String gender = req.getParameter("gender");
        if (gender != null){
            player.setGender(Gender.valueOf(gender));
        } else {
            player.setGender(null);
        }

        String date = req.getParameter("date");
        if (date.length() != 0){
            int y = Integer.parseInt(date.substring(0, 4));
            int m = Integer.parseInt(date.substring(5, 7));
            int d = Integer.parseInt(date.substring(8, 10));

            player.setBirthdayDate(LocalDate.of(y, m, d));
        }

        String about = req.getParameter("about");
        if (!about.equals("")) {
            player.setAbout(about);
        }

        player.setRegistrationTime(LocalDateTime.now());

        playerManager.registerNewPlayer(player);

        logger.info(String.format("player %s registered", player.getLogin()));

        player = playerManager.getPlayerByLogin(login);
        req.getSession().setAttribute("player", player);

        resp.sendRedirect("gameList");
    }
}
