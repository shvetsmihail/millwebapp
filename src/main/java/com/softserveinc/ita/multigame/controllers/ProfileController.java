package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.managers.PlayerManager;
import com.softserveinc.ita.multigame.model.managers.impl.PlayerManagerImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/profile/*")
public class ProfileController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private PlayerManager playerManager = PlayerManagerImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Logger logger = Logger.getLogger(ProfileController.class);

        String login = req.getParameter("login");
        Player player = playerManager.getPlayerByLogin(login);

        logger.info(String.format("%s's profile was read", player.getLogin()));

        Player currentPlayer = (Player) req.getSession().getAttribute("player");
        req.setAttribute("player", player);

        if (player.equals(currentPlayer)){
            req.getRequestDispatcher("/myProfile.jsp").forward(req, resp);
        } else {
            req.getRequestDispatcher("/profile.jsp").forward(req, resp);
        }









    }
}
