package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.managers.GameManager;
import com.softserveinc.ita.multigame.model.managers.impl.GameManagerImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/makeTurn/*")
public class MakeTurnController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private GameManager gameManager = GameManagerImpl.getInstance();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Logger logger = Logger.getLogger(MakeTurnController.class);

        Long id = Long.parseLong(req.getParameter("id"));
        String p1 = req.getParameter("p1");
        String p2 = req.getParameter("p2");

        Player player = (Player) req.getSession().getAttribute("player");
        Game game = gameManager.getGameById(id);

        game.makeTurn(player, p1, p2);

        logger.info(String.format("player %s in game #%d made turn p1=%s p2=%s", player.getLogin(), id, p1, p2));
        resp.sendRedirect(String.format("game?id=%s", id));
    }

}
