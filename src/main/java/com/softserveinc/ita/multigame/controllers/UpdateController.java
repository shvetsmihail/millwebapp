package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Gender;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.managers.PlayerManager;
import com.softserveinc.ita.multigame.model.managers.impl.PlayerManagerImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet("/update")
public class UpdateController extends HttpServlet {
    private PlayerManager playerManager = PlayerManagerImpl.getInstance();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Logger logger = Logger.getLogger(RegisterController.class);

        Player player = (Player) req.getSession().getAttribute("player");

        String email = req.getParameter("email");
        player.setEmail(email);

        String fullName = req.getParameter("fullName");
        if (!fullName.equals("")) {
            player.setFullName(fullName);
        }

        String gender = req.getParameter("gender");
        if (gender != null){
            player.setGender(Gender.valueOf(gender));
        } else {
            player.setGender(null);
        }

        String date = req.getParameter("date");
        if (date.length() != 0){
            int y = Integer.parseInt(date.substring(0, 4));
            int m = Integer.parseInt(date.substring(5, 7));
            int d = Integer.parseInt(date.substring(8, 10));
            player.setBirthdayDate(LocalDate.of(y, m, d));
        }

        String about = req.getParameter("about");
        if (!about.equals("")) {
            player.setAbout(about);
        }


        playerManager.updatePlayer(player);

        logger.info(String.format("player %s registered", player.getLogin()));

        player = playerManager.getPlayerByLogin(player.getLogin());

        resp.sendRedirect("profile?login="+player.getLogin());
    }
}
