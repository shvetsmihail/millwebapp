package com.softserveinc.ita.multigame.controllers;


import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.managers.GameManager;
import com.softserveinc.ita.multigame.model.managers.impl.GameManagerImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/gameList")
public class GameListController extends HttpServlet{
    private static final long serialVersionUID = 1L;
    private GameManager gameManager = GameManagerImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Logger logger = Logger.getLogger(GameListController.class);

        Player player = (Player) req.getSession().getAttribute("player");

        req.setAttribute("playingGames", gameManager.getPlayingGameIDs(player));
        req.setAttribute("createdGames", gameManager.getCreatedGameIDs(player));
        req.setAttribute("waitingGames", gameManager.getWaitingGameIDs(player));

        logger.info(String.format("player %s update gameList", player.getLogin()));
        req.getRequestDispatcher("/gameList.jsp").forward(req, resp);

    }
}
