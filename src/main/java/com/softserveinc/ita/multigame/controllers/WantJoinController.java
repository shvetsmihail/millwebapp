package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.Game;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.managers.GameManager;
import com.softserveinc.ita.multigame.model.managers.impl.GameManagerImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/wantJoin")
public class WantJoinController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/wantJoin.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GameManager gameManager = GameManagerImpl.getInstance();
        Logger logger = Logger.getLogger(WantJoinController.class);

        Long id = Long.parseLong(req.getParameter("id"));
        Player player = (Player) req.getSession().getAttribute("player");

        Game game = gameManager.getGameById(id);
        game.joinToGame(player);
        logger.info(String.format("player %s joined to game #%d", player.getLogin(), id));
        //req.getRequestDispatcher(String.format("game?id=%s", id)).forward(req, resp);
        resp.sendRedirect(String.format("game?id=%s", id));
    }
}
