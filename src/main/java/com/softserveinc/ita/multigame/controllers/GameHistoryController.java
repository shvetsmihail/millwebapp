package com.softserveinc.ita.multigame.controllers;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.TurnHistory;
import com.softserveinc.ita.multigame.model.managers.GameManager;
import com.softserveinc.ita.multigame.model.managers.impl.GameManagerImpl;
import com.softserveinc.ita.multigame.services.GsonParserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/gameHistory/*")
public class GameHistoryController extends HttpServlet {
    private GameManager gameManager = GameManagerImpl.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("id"));
        GameHistory gameHistory = gameManager.getGameHistory(id);
        Player p1 = gameHistory.getFirstPlayer();
        Player p2 = gameHistory.getSecondPlayer();

        List<String> turns = new ArrayList<>();
        for (String turn : gameHistory.getTurns()) {
            TurnHistory th = (TurnHistory) GsonParserService.fromJson(turn, TurnHistory.class);

            Player p;
            if (th.getPlayerId().equals(p1.getId())) {
                p = p1;
            } else {
                p = p2;
            }

            turns.add(String.format("turn: %s\t player: %s\t date: %s",
                    th.getTurn(), p.getLogin(), th.getTime().format(DateTimeFormatter.ISO_DATE_TIME)));
        }

        String winner="";
        switch (gameHistory.getResultStatus()) {
            case FIRST:
                winner = p1.getLogin();
                break;
            case SECOND:
                winner = p2.getLogin();
                break;
        }


        req.setAttribute("id", gameHistory.getGameId());
        req.setAttribute("firstPlayer", p1.getLogin());
        req.setAttribute("secondPlayer", p2.getLogin());
        req.setAttribute("winner", winner);
        req.setAttribute("turns", turns);
        req.getRequestDispatcher("/gameHistory.jsp").forward(req, resp);
    }
}
