package com.softserveinc.ita.multigame.dao;

import javax.persistence.EntityManager;

public interface MyEntityManagerFactory {
    EntityManager getEntityManager();
    void close();
}
