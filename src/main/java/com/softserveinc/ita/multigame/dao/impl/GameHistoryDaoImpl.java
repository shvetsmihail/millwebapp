package com.softserveinc.ita.multigame.dao.impl;

import com.softserveinc.ita.multigame.dao.GameHistoryDao;
import com.softserveinc.ita.multigame.dao.MyEntityManagerFactory;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

public class GameHistoryDaoImpl implements GameHistoryDao {
    private MyEntityManagerFactory emf = PGEntityManagerFactoryImp.getInstance();

    @Override
    public boolean save(GameHistory gameHistory) {
        if (gameHistory == null) return false;
        EntityManager entityManager = emf.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(gameHistory);
            entityManager.getTransaction().commit();
            entityManager.close();
            return true;
        } catch (PersistenceException e) {
            entityManager.close();
            return false;
        }
    }

    @Override
    public boolean delete(GameHistory gameHistory) {
        EntityManager entityManager = emf.getEntityManager();
        if (gameHistory != null && gameHistory.getGameId() != null
                && entityManager.find(GameHistory.class, gameHistory.getGameId()) != null) {
            entityManager.getTransaction().begin();
            entityManager.remove(entityManager.merge(gameHistory));
            entityManager.getTransaction().commit();
            entityManager.close();
            return true;
        }
        entityManager.close();
        return false;

    }

    @Override
    public GameHistory getById(Long id) {
        if (id == null) return null;
        EntityManager entityManager = emf.getEntityManager();
        GameHistory gh = entityManager.find(GameHistory.class, id);
        gh.getTurns().isEmpty();
        entityManager.close();
        return gh;
    }

    @Override
    public List<GameHistory> getAllGames(Player player) {
        if (player == null) return null;
        EntityManager entityManager = emf.getEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GameHistory> criteriaQuery = criteriaBuilder.createQuery(GameHistory.class);
        Root<GameHistory> gameHistoryRoot = criteriaQuery.from(GameHistory.class);
        Predicate condition1 = criteriaBuilder.equal(gameHistoryRoot.get("firstPlayer"), player.getId());
        Predicate condition2 = criteriaBuilder.equal(gameHistoryRoot.get("secondPlayer"), player.getId());
        criteriaQuery.where(criteriaBuilder.or(condition1, condition2));
        TypedQuery<GameHistory> typedQuery = entityManager.createQuery(criteriaQuery);
        List<GameHistory> gameHistories = typedQuery.getResultList();
        entityManager.close();
        return gameHistories;
    }

}
