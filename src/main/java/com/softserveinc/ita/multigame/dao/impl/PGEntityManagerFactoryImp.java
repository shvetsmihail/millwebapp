package com.softserveinc.ita.multigame.dao.impl;

import com.softserveinc.ita.multigame.dao.MyEntityManagerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PGEntityManagerFactoryImp implements MyEntityManagerFactory {
    private EntityManagerFactory emf;

    private PGEntityManagerFactoryImp() {
        emf = Persistence.createEntityManagerFactory("PG");
    }

    private static class SingletonHolder {
        static final PGEntityManagerFactoryImp HOLDER_INSTANCE = new PGEntityManagerFactoryImp();
    }

    public static PGEntityManagerFactoryImp getInstance() {
        return PGEntityManagerFactoryImp.SingletonHolder.HOLDER_INSTANCE;
    }

    @Override
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public void close() {
        emf.close();
    }


}
