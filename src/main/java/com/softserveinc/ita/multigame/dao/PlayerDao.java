package com.softserveinc.ita.multigame.dao;


import com.softserveinc.ita.multigame.model.Player;

import java.util.List;

public interface PlayerDao {
    boolean save(Player player);
    boolean update(Player player);
    boolean delete(Player player);
    Player getById(Long id);
    Player getByLogin(String login);
    List<Player> getAll();

}
