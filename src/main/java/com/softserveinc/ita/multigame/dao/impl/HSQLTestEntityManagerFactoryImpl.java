package com.softserveinc.ita.multigame.dao.impl;

import com.softserveinc.ita.multigame.dao.MyEntityManagerFactory;
import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.ResultStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class HSQLTestEntityManagerFactoryImpl implements MyEntityManagerFactory {
    private EntityManagerFactory emf;

    private HSQLTestEntityManagerFactoryImpl() {
        emf = Persistence.createEntityManagerFactory("HSQL");
        init();
    }

    private static class SingletonHolder {
        static final HSQLTestEntityManagerFactoryImpl HOLDER_INSTANCE = new HSQLTestEntityManagerFactoryImpl();
    }

    public static HSQLTestEntityManagerFactoryImpl getInstance() {
        return HSQLTestEntityManagerFactoryImpl.SingletonHolder.HOLDER_INSTANCE;
    }

    @Override
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public void close() {
        emf.close();
    }

    private void init() {
        Player p1 = new Player("Vasya", "111", "v@gmail.com");
        Player p2 = new Player("Petya", "111", "p@gmail.com");
        Player p3 = new Player("Kolya", "111", "v@gmail.com");
        Player p4 = new Player("Sasha", "111", "p@gmail.com");

        List<String> turns = new ArrayList<>();
        turns.add(String.format("time: %s, player: %s, game #%s, turn: %s", LocalDateTime.now(), p1.getLogin(), 1, "put 1"));
        turns.add(String.format("time: %s, player: %s, game #%s, turn: %s", LocalDateTime.now(), p2.getLogin(), 1, "put 2"));

        GameHistory g = new GameHistory(1L, p1, p2, LocalDateTime.now(), LocalDateTime.now(), turns, ResultStatus.DRAWN);

        EntityManager entityManager = getEntityManager();

        entityManager.getTransaction().begin();

        entityManager.persist(p1);
        entityManager.persist(p2);
        entityManager.persist(g);

        entityManager.getTransaction().commit();
        entityManager.close();

    }
}
