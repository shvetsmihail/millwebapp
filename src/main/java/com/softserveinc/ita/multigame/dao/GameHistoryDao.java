package com.softserveinc.ita.multigame.dao;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;

import java.util.List;

public interface GameHistoryDao {
    boolean save(GameHistory gameHistory);
    boolean delete(GameHistory gameHistory);
    GameHistory getById(Long gameId);
    List<GameHistory> getAllGames(Player player);
}
