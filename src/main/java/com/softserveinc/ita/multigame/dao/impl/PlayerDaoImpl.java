package com.softserveinc.ita.multigame.dao.impl;

import com.softserveinc.ita.multigame.dao.MyEntityManagerFactory;
import com.softserveinc.ita.multigame.dao.PlayerDao;
import com.softserveinc.ita.multigame.model.Player;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

public class PlayerDaoImpl implements PlayerDao {
    private MyEntityManagerFactory emf = PGEntityManagerFactoryImp.getInstance();

    @Override
    public boolean save(Player player) {
        if (player == null) return false;
        EntityManager entityManager = emf.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(player);
            entityManager.getTransaction().commit();
            entityManager.close();
            return true;
        } catch (PersistenceException e) {
            entityManager.close();
            return false;
        }

    }

    @Override
    public boolean update(Player player) {
        EntityManager entityManager = emf.getEntityManager();
        if (player != null && player.getId() != null && entityManager.find(Player.class, player.getId()) != null) {
            entityManager.getTransaction().begin();
            entityManager.merge(player);
            entityManager.getTransaction().commit();
            entityManager.close();
            return true;
        }
        entityManager.close();
        return false;
    }

    @Override
    public boolean delete(Player player) {
        EntityManager entityManager = emf.getEntityManager();
        if (player != null && player.getId() != null && entityManager.find(Player.class, player.getId()) != null) {
            entityManager.getTransaction().begin();
            entityManager.remove(entityManager.merge(player));
            entityManager.getTransaction().commit();
            entityManager.close();
            return true;
        }
        entityManager.close();
        return false;

    }

    @Override
    public Player getById(Long id) {
        if (id == null) return null;
        EntityManager entityManager = emf.getEntityManager();
        Player p = entityManager.find(Player.class, id);
        entityManager.close();
        return p;
    }


    @Override
    public Player getByLogin(String login) {
        if (login == null) return null;
        EntityManager entityManager = emf.getEntityManager();
        Player p = null;
        try {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Player> criteriaQuery = criteriaBuilder.createQuery(Player.class);
            Root<Player> playerRoot = criteriaQuery.from(Player.class);
            Predicate condition = criteriaBuilder.equal(playerRoot.get("login"), login);
            criteriaQuery.where(condition);
            TypedQuery<Player> typedQuery = entityManager.createQuery(criteriaQuery);

            p = typedQuery.getSingleResult();
            entityManager.close();
        } catch (NoResultException e) {
            entityManager.close();
        }
        return p;
    }

    @Override
    public List<Player> getAll() {
        EntityManager entityManager = emf.getEntityManager();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Player> criteriaQuery = criteriaBuilder.createQuery(Player.class);
        Root<Player> playerRoot = criteriaQuery.from(Player.class);
        TypedQuery<Player> typedQuery = entityManager.createQuery(criteriaQuery);

        List<Player> players = typedQuery.getResultList();
        entityManager.close();
        return players;
    }
}
