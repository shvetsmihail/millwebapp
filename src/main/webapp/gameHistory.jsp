<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MillGame profile</title>
    <style>
        @import "css/gameHistory.css";
    </style>
</head>

<body>
<h1>Game#${id}</h1>

<h2><a href="profile?login=${firstPlayer}">${firstPlayer}</a> vs
    <a href="profile?login=${secondPlayer}">${secondPlayer}</a></h2>

<h2>Winner is ${winner}</h2>
<textarea id="turns" readonly wrap="off">
<%
    for (String turn: (List<String>)request.getAttribute("turns")){
        out.println(turn);
    }
%>
</textarea>
<div class="button_container">
    <form method="get" action="profile">
        <input type="hidden" name="login" value=${player.getLogin()}>
        <input type="submit" id="back_btn" value="Back">
    </form>
</div>
</body>
</html>

