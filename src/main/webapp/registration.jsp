<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MillGame registration</title>
    <style>
        @import "css/registration.css";
    </style>
</head>

<body>
<h1>Registration</h1>
<form id="registration" method="post" action="registration">
    <div id="info">
        <input class="text" type="text" name="login" placeholder="login" required>
        <input class="text" type="password" name="password" placeholder="password" required>
        <input class="text" type="email" name="email" placeholder="email" required>


        <input class="text" type="text" name="fullName" placeholder="fullName">

        <div id="gender"> Gender:
            <input type="radio" name="gender" value="MALE"> Male
            <input type="radio" name="gender" value="FEMALE"> Female
        </div>


        <div id="birthday"> Birthday:
            <input type="date" id="date" name="date"/>
        </div>

        <textarea id="about" name="about" placeholder="about"></textarea>

    </div>
    <input type="submit" id="signUpBtn" value="Sign up">
</form>

<form method="get" action="login">
    <input type="submit" id="backBtn" value="Back">
</form>

</body>
</html>