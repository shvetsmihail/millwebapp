<%@ page import="com.softserveinc.ita.multigame.model.GameHistory" %>
<%@ page import="com.softserveinc.ita.multigame.model.Gender" %>
<%@ page import="com.softserveinc.ita.multigame.model.Player" %>
<%@ page import="java.util.List" %>
<%@ page import="com.softserveinc.ita.multigame.model.managers.impl.GameManagerImpl" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MillGame profile</title>
    <style>
        @import "css/myProfile1.css";
    </style>
</head>

<body>
<h1>Player ${player.getLogin()}</h1>


<div id="text">
    <form method="post" action="update">
        <%
            Player player = (Player) request.getAttribute("player");

            out.println("<div class=\"label\">Email:</div>");
            out.println(String.format("<input class=\"text\" type=\"text\" name=\"email\" required value =%s>",
                    player.getEmail()));

            out.println("<div class=\"label\">Full name:</div>");
            out.println(String.format("<input class=\"text\" type=\"text\" name=\"fullName\" placeholder=\"full name\" value =%s>",
                    player.getFullName() == null ? "" : player.getFullName()));

            out.println("<div class=\"label\">Gender:</div>");
            out.println("<div id=\"gender\">");
            if (player.getGender() == null) {
                out.println("<input type=\"radio\" name=\"gender\" value=\"MALE\"> Male" +
                        "<input type=\"radio\" name=\"gender\" value=\"FEMALE\"> Female");
            } else if (player.getGender() == Gender.MALE) {
                out.println("<input type=\"radio\" name=\"gender\" value=\"MALE\" checked> Male" +
                        "<input type=\"radio\" name=\"gender\" value=\"FEMALE\"> Female");
            } else if (player.getGender() == Gender.FEMALE) {
                out.println("<input type=\"radio\" name=\"gender\" value=\"MALE\"> Male" +
                        "<input type=\"radio\" name=\"gender\" value=\"FEMALE\" checked> Female");
            }
            out.println("</div>");

            out.println("<div class=\"label\">Birthday:</div>");
            out.println(String.format("<div id=\"birthday\">" +
                            "<input type=\"date\" id=\"date\" name=\"date\" value =%s>" +
                            "</div>",
                    player.getBirthdayDate() == null ? "" : player.getBirthdayDate()));

            out.println("<div class=\"label\">About:</div>");
            out.println(String.format("<textarea id=\"about\" name=\"about\" placeholder=\"about\">%s</textarea>",
                    player.getAbout() == null ? "" : player.getAbout()));

        %>

        <input type="submit" id="save_btn" value="Save">
    </form>

    <form method="post" action="gameList">
        <input type="submit" id="back_btn" value="Game List">
    </form>
</div>

<div id="game_history">
    <h2>Finished games</h2>
    <ol>
        <%
            List<GameHistory> histories = GameManagerImpl.getInstance().getGameHistories(player);
            for (GameHistory gh : histories) {
                out.println(String.format("<li><a href=\"gameHistory?id=%s\">Game #%s: %s vs %s </a></li>",
                        gh.getGameId(), gh.getGameId(),
                        gh.getFirstPlayer().getLogin(),
                        gh.getSecondPlayer().getLogin()));
            }
        %>
    </ol>

</div>


</body>
</html>