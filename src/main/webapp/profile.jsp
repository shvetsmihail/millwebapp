<%@ page import="com.softserveinc.ita.multigame.model.Player" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MillGame profile</title>
    <style>
        @import "css/profile.css";
    </style>
</head>

<body>
<h1>Player ${player.getLogin()}</h1>
<div id="text">
    <%
        Player player = (Player) request.getAttribute("player");

        if (player.getFullName() != null) {
            out.println(String.format("<div class=\"text\"><em>Full name: </em>%s</div>", player.getFullName()));
        }

        if (player.getEmail() != null) {
            out.println(String.format("<div class=\"text\"><em>Email: </em>%s</div>", player.getEmail()));
        }

        if (player.getBirthdayDate() != null) {
            out.println(String.format("<div class=\"text\"><em>Birthday: </em>%s</div>", player.getBirthdayDate()));
        }

        if (player.getGender() != null) {
            out.println(String.format("<div class=\"text\"><em>Gender: </em>%s</div>", player.getGender()));
        }

        if (player.getAbout() != null) {
            out.println(String.format("<div class=\"text\"><em>About: </em>%s</div>", player.getAbout()));
        }

    %>
</div>

<div class="button_container">
    <form method="post" action="gameList">
        <input type="submit" id="back_btn" value="Game List">
    </form>
</div>
</body>
</html>