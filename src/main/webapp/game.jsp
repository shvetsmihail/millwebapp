<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MillGame game</title>
    <style>
        @import "css/game.css";
    </style>
    <script src="js/dragDropClick.js"></script>
    <script src="js/main.js"></script>
    <script src="js/refreshOnce.js"></script>
    <script src="js/refreshService.js"></script>
    <script src="js/elements.js"></script>
</head>

<body>
<h1>Game #${id}</h1>
<h2>${message} <a href="profile?login=${opponent}">${opponent}</a></h2>

<div class="turn_container">
    <div id="tips"></div>
    <input id="gameId" type="hidden" name="id" value=${id}>
    <form method="post" action="gameList">
        <input type="submit" id="back_btn" value="Game List">
    </form>
</div>

<div class="field_container">

    <div id="white"></div>

    <div id="board">
        <div class="position" id="0" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="1" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="2" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position" id="8" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position" id="9" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position" id="10" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="16" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position" id="17" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position" id="18" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="7" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position" id="15" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position" id="23" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position" id="19" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position" id="11" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position" id="3" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="22" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position" id="21" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position" id="20" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="14" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position" id="13" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position" id="12" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position" id="6" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="5" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
        <div class="position"></div>
        <div class="position"></div>
        <div class="position" id="4" onclick="getId(this)" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
    </div>

    <div id="black"></div>

</div>

</body>
</html>