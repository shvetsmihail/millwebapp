function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("stone", ev.target.parentNode.id);
}

function drop(ev) {
    var p1 = ev.dataTransfer.getData("stone");
    var p2 = ev.target.id;
    makeTurn(p1, p2);
    
}

function getId(el) {
    makeTurn(el.id);
}

function getPatentId(id) {
    alert("parent id:" + id);
    makeTurn(id);
}


function makeTurn(p1, p2) {
    var url;
    var gameId = document.getElementById('gameId').value;
    if (p2 == undefined){
        url = 'makeTurn?id=' + gameId + '&p1=' + p1;
    } else {
        url = 'makeTurn?id=' + gameId + '&p1=' + p1 + '&p2=' + p2;
    }

    var ajax = new XMLHttpRequest();

    ajax.open('GET', url, true);
    ajax.send();
    refreshOnce();
}


