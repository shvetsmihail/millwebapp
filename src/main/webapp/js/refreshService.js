'use strict';

function refreshLoop(object, restart) {

    var ajax;

    function request() {
        ajax = new XMLHttpRequest();

        ajax.open('GET', object.url, true);
        ajax.send();

        ajax.addEventListener('readystatechange', function () {
            if (ajax.readyState < 4)
                return;

            if (ajax.status != 200) {
                errorResult();
            } else {
                successResult();
            }
        }, false);
    }

    function errorResult() {
        alert(ajax.status + ': ' + ajax.statusText);
    }

    function successResult() {
        object.refresh(ajax.responseText);
        if (restart) {
            setTimeout(request(), 5000);
        }
    }

    function _start() {
        request();
    }

    return {
        start: _start
    };
}



