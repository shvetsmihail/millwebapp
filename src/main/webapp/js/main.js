'use strict';

window.addEventListener('load', main, false);

function main() {
    var elements = getElements();
    for (var i = 0; i < elements.length; i++) {
        refreshLoop(elements[i], true).start();
    }
}