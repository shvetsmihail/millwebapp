'use strict';

function getElements() {

    var gameId = document.getElementById('gameId').value;

    var board = {
        url: 'refreshBoard?id=' + gameId,
        refresh: function (responseText) {
            var jsonData = JSON.parse(responseText);

            var board = jsonData.stonesOnBoard;
            var countOfWhite = jsonData.firstPlayerCountOfStoneInHand;
            var countOfBlack = jsonData.secondPlayerCountOfStoneInHand;
            
            for (var i = 0; i < 24; i++) {
                if (board[i] != "none") {
                    document.getElementById("" + i).innerHTML = '<div  id="' + "s" + i + '" class="' + board[i] 
                        + '" onclick="getPatentId(i)" draggable="true" ondragstart ="drag(event)"></div>';
                } else {
                    document.getElementById("" + i).innerHTML = "";
                }
            }

            var white = '', black = '';
            while (countOfBlack > 0){
                black += '<div class="blackStone"></div>';
                countOfBlack--;
            }
            document.getElementById('black').innerHTML = black;
            
            while (countOfWhite > 0){
                white += '<div class="whiteStone"></div>';
                countOfWhite--;
            }
            document.getElementById('white').innerHTML = white;
        }
    };

    var tips = {
        url: 'refreshTips?id=' + gameId,
        refresh: function (responseText) {
            document.getElementById('tips').innerHTML = responseText;
        }
    };

    // var stones = {
    //     url: 'refreshStones?id=' + gameId,
    //     refresh: function (responseText) {
    //         var jsonData = JSON.parse(responseText);
    //         var white = '', black = '';
    //         for (var i = 0; i < jsonData.length; i++) {
    //             if (jsonData[i] == "W") {
    //                 white += '<div class="whiteStone"></div>';
    //             } else {
    //                 black += '<div class="blackStone"></div>';
    //             }
    //
    //             document.getElementById('white').innerHTML = white;
    //             document.getElementById('black').innerHTML = black;
    //         }
    //     }
    // };
    
    return [board, tips];
}

