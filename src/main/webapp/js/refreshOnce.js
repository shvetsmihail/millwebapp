'use strict';

function refreshOnce() {
    var elements = getElements();
    for (var i = 0; i < elements.length; i++) {
        refreshLoop(elements[i], false).start();
    }
}