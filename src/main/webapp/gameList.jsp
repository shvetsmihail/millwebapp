﻿<%@ page import="java.util.List" %>
<%@ page import="com.softserveinc.ita.multigame.model.managers.impl.GameManagerImpl" %>
<%@ page import="com.softserveinc.ita.multigame.model.Player" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="refresh" content="5">
    <title>MillGame game list</title>
    <style>
        @import "css/gameList.css";
    </style>
</head>

<body>

<h1>${player.getLogin()}'s Game List</h1>

<div class="buttons_container">
    <form method="post" action="createGame">
        <input type="submit" id="create_btn" value="Create">
    </form>
    <form method="get" action="profile">
        <input type="hidden" name="login" value="${player.getLogin()}">
        <input type="submit" id="profile_btn" value="Profile">
    </form>
    <form method="get" action="exit">
        <input type="submit" id="back_btn" value="Exit">
    </form>
</div>

<div class="play_container">
    <h2>Playing game</h2>
    <ol>
        <%
            for (Long id : (List<Long>) request.getAttribute("playingGames")) {

                Player player = (Player) request.getSession().getAttribute("player");

                Player opponent = GameManagerImpl.getInstance().getGameById(id).getFirstPlayer();
                if (opponent.equals(player)) {
                    opponent = GameManagerImpl.getInstance().getGameById(id).getSecondPlayer();
                }
                out.println(String.format("<li><a href=\"game?id=%s\">Game # %s <em>%s</em></a></li>", id, id, opponent.getLogin()));
            }
        %>
    </ol>
</div>

<div class="create_container">
    <h2>Created game</h2>
    <ol>
        <%
            for (Long id : (List<Long>) request.getAttribute("createdGames")) {
                out.println(String.format("<li><a href=\"game?id=%s\">Game # %s</a></li>", id, id));
            }
        %>
    </ol>
</div>

<div class="wait_container">
    <h2>Waiting game</h2>
    <ol>
        <%
            for (Long id : (List<Long>) request.getAttribute("waitingGames")) {
                Player owner = GameManagerImpl.getInstance().getGameById(id).getFirstPlayer();
                out.println(String.format("<li><a href=\"game?id=%s\">Game # %s <em>%s</em></a></li>", id, id, owner.getLogin()));
            }
        %>
    </ol>
</div>

</body>
</html>